# Metadata

Manage data via JSON, so it's easy to contribute for everybody and easy for programs to parse.

Include:

- Platform data (Schema: [platform_metadata_schema.json](./platform_metadata_schema.json))
- Icons

## Add / Update a platform

just fork this repo, update [platform_metadata.json](./platform_metadata.json) and<br/>
execute `./tool_sort-json.sh` to have it formatted and sorted correctly.

If you add/cange an **"icon_url"** also execute `./tool_download_icons.sh`.

Commit all changes and create a new pull request.
