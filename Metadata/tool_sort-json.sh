#!/bin/bash

[ "$1" == "--check" ] && {
  DIFF="$(diff platform_metadata.json <(cat platform_metadata.json | jq 'sort_by(.name)') | head -n1)"
  [ "$DIFF" != "" ] && echo "json not sorted" && exit 1
  exit 0
}

cp platform_metadata.json platform_metadata.json.bak && \
  cat platform_metadata.json.bak  | jq 'sort_by(.name)' > platform_metadata.json && \
  rm platform_metadata.json.bak
