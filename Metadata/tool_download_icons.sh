#!/bin/bash

icon_list=$(cat platform_metadata.json  | jq '.[] | {name,icon_url} | select(.icon_url != "") |  select(.icon_url != null)')

echo ${icon_list} | jq -c | while read -r line; do
  name=$(echo "$line" | jq -r '.name')
  icon_url=$(echo "$line" | jq -r '.icon_url')

  if [ -f "./Icons/$name."??? ]; then
    echo "skip '$name' icon exist"
  else

    wget -O ./Icons/"${name}" "$icon_url"

    ext=$(file --extension ./Icons/"${name}" | cut -d ' ' -f2)
    case $ext in
      "svg"|"png"|"jpg")
        mv -f ./Icons/"${name}" ./Icons/"${name}.${ext}"
        ;;
      *)
        convert ./Icons/"${name}" ./Icons/"${name}".png
        rm ./Icons/"${name}"
        ;;
    esac

  fi
done
