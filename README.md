# **NEXT** The-Federation.info

[![Join the Matrix room](https://img.shields.io/matrix/the-federation:matrix.org?label=matrix)](https://matrix.to/#/#the-federation:matrix.org)
[![](https://img.shields.io/badge/license-AGPLv3-green.svg)](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0))

<a href="https://codeberg.org/thefederationinfo/next">
    <img alt="Get it on Codeberg" src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-neon-blue.png" height="60">
</a>

---

Tracking various projects around the [fediverse](https://fediverse.party/) using ActivityPub, Matrix, Diaspora and other protocols.<br/>
By visualizing statistics crawled via [NodeInfo](http://nodeinfo.diaspora.software/) endpoint.

<!--
Site found at: https://next.the-federation.info
-->

## How to get your platform listed

Just implement the [`.well-known/nodeinfo`](http://nodeinfo.diaspora.software/) endpoint to your project.

Matrix instances are scraped with a dedicated scraper since they don't (yet) provide generic metadata.

<!-- TODO(7): define view route and implement it
### My just an instance admin, and my platform support it already

Register your node, by adding it at https://the-federation.info/XXX`.
-->

### My platform misses information

Additional things like: **Icon**, Description, Display Name, License, ...<br/>
Are managed by a [json file](./Metadata/platform_metadata.json) [here](./Metadata/README.md).

### Code of Conduct

While interactions on our site is not possible, we expect sites we list to have a humane code of conduct in place. Should sites who fail to ban content that can be found generally harmful, that node will be blocked from listing here.

Harmful content can be, but not limited to, malware, graphical material of minorities, abusive images, hateful content, racist content and climate denialism. The admins of this site reserve the right to decide case by case on blocking of nodes.

**Please report any nodes violating our terms.**

## Development

### Architecture

We use microservice architecture to scale as we have to.

Also this allows us to pick the right tool/language for the right job.

### Structure

- [WebUI](./WebUI/README.md): Frontend delivery
- [Database](./Database/README.md): GraphQL engine and state
- [Registrar](./Registrar/README.md): Gatekeeper for adding new Nodes.
- [cRawler](./cRawler/README.md): Service for updating Data by crawling
- [Metadata](./Metadata/README.md): JSON data and tools to manage it

```mermaid
graph LR;
  WebUI-.->Database
  WebUI-.->Metadata
  WebUI-->Registrar
  Registrar-->cRawler
  cRawler-->Database
  cRawler-.->Metadata
```

_* doted line is **read access**; full line is **write access**;_

### Notes

times are UTC as to when we have to craw new and what day we use to calc stats etc ...
