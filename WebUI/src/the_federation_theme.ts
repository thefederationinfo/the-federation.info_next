// You can also use the generator at https://skeleton.dev/docs/generator to create these values for you
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';
export const the_federation_theme: CustomThemeConfig = {
  name: 'the_federation_theme',
  properties: {
    // =~= Theme Properties =~=
    '--theme-font-family-base': 'system-ui',
    '--theme-font-family-heading': 'system-ui',
    '--theme-font-color-base': '0 0 0',
    '--theme-font-color-dark': '255 255 255',
    '--theme-rounded-base': '2px',
    '--theme-rounded-container': '4px',
    '--theme-border-base': '1px',
    // =~= Theme On-X Colors =~=
    '--on-primary': '0 0 0',
    '--on-secondary': '0 0 0',
    '--on-tertiary': '0 0 0',
    '--on-success': '0 0 0',
    '--on-warning': '0 0 0',
    '--on-error': '0 0 0',
    '--on-surface': '255 255 255',
    // =~= Theme Colors  =~=
    // primary | #00829e
    '--color-primary-50': '206 244 255', // #CEF4FF
    '--color-primary-100': '166 216 231', // #A6D8E7
    '--color-primary-200': '130 190 207', // #82BECF
    '--color-primary-300': '99 165 183', // #63A5B7
    '--color-primary-400': '72 140 159', // #488C9F
    '--color-primary-500': '49 117 136', // #317588
    '--color-primary-600': '30 95 112', // #1E5F70
    '--color-primary-700': '16 73 88', // #104958
    '--color-primary-800': '6 52 64', // #063440
    '--color-primary-900': '0 32 40', // #002028
    // secondary | #617c84
    '--color-secondary-50': '217 246 255', // #D9F6FF
    '--color-secondary-100': '176 218 231', // #B0DAE7
    '--color-secondary-200': '139 192 207', // #8BC0CF
    '--color-secondary-300': '107 166 182', // #6BA6B6
    '--color-secondary-400': '79 142 158', // #4F8E9E
    '--color-secondary-500': '55 118 134', // #377686
    '--color-secondary-600': '35 95 110', // #235F6E
    '--color-secondary-700': '20 73 85', // #144955
    '--color-secondary-800': '9 52 61', // #09343D
    '--color-secondary-900': '2 31 37', // #021F25
    // tertiary | #717598
    '--color-tertiary-50': '237 238 242', // #EDEEF2
    '--color-tertiary-100': '207 209 218', // #CFD1DA
    '--color-tertiary-200': '178 180 194', // #B2B4C2
    '--color-tertiary-300': '151 153 169', // #9799A9
    '--color-tertiary-400': '125 127 145', // #7D7F91
    '--color-tertiary-500': '100 102 121', // #646679
    '--color-tertiary-600': '77 79 97', // #4D4F61
    '--color-tertiary-700': '56 56 72', // #383848
    '--color-tertiary-800': '36 36 48', // #242430
    '--color-tertiary-900': '17 17 24', // #111118
    // success | #84cc16
    '--color-success-50': '242 252 227', // #F2FCE3
    '--color-success-100': '231 250 204', // #E7FACC
    '--color-success-200': '208 244 154', // #D0F49A
    '--color-success-300': '184 239 103', // #B8EF67
    '--color-success-400': '159 233 48', // #9FE930
    '--color-success-500': '132 204 22', // #84CC16
    '--color-success-600': '104 161 17', // #68A111
    '--color-success-700': '80 124 13', // #507C0D
    '--color-success-800': '53 83 9', // #355309
    '--color-success-900': '27 41 4', // #1B2904
    // warning | #EAB308
    '--color-warning-50': '254 248 230', // #FEF8E6
    '--color-warning-100': '253 241 206', // #FDF1CE
    '--color-warning-200': '251 226 151', // #FBE297
    '--color-warning-300': '250 213 102', // #FAD566
    '--color-warning-400': '248 198 48', // #F8C630
    '--color-warning-500': '234 179 8', // #EAB308
    '--color-warning-600': '187 142 6', // #BB8E06
    '--color-warning-700': '143 108 5', // #8F6C05
    '--color-warning-800': '94 71 3', // #5E4703
    '--color-warning-900': '49 37 2', // #312502
    // error | #BA1A1A
    '--color-error-50': '251 228 228', // #FBE4E4
    '--color-error-100': '248 201 201', // #F8C9C9
    '--color-error-200': '241 152 152', // #F19898
    '--color-error-300': '233 98 98', // #E96262
    '--color-error-400': '226 45 45', // #E22D2D
    '--color-error-500': '186 26 26', // #BA1A1A
    '--color-error-600': '148 21 21', // #941515
    '--color-error-700': '112 16 16', // #701010
    '--color-error-800': '76 11 11', // #4C0B0B
    '--color-error-900': '36 5 5', // #240505
    // surface | #757779
    '--color-surface-50': '232 242 246', // #E8F2F6
    '--color-surface-100': '202 216 222', // #CAD8DE
    '--color-surface-200': '173 191 198', // #ADBFC6
    '--color-surface-300': '146 167 175', // #92A7AF
    '--color-surface-400': '121 143 151', // #798F97
    '--color-surface-500': '97 119 127', // #61777F
    '--color-surface-600': '76 96 103', // #4C6067
    '--color-surface-700': '55 73 80', // #374950
    '--color-surface-800': '37 51 56', // #253338
    '--color-surface-900': '20 29 32', // #141D20
  },
};
