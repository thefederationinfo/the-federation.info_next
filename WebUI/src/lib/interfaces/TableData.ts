export interface TableData<T> {
  data: T[];
  headers: {
    orderBy?: string;
    label: string;
  }[];
}
