import type { Writable } from 'svelte/store';

export interface LayerCakeContext {
  data: Writable<[number, number][]>;
  xGet: Writable<(d: [number, number] | object) => number>;
  yGet: Writable<(d: [number, number] | object) => number>;
  width: Writable<number>;
  height: Writable<number>;
  yScale: Writable<(value: number) => number>;
  config: Writable<LayerCakeConfig>;
}

interface LayerCakeConfig {
  x: number | string;
}
