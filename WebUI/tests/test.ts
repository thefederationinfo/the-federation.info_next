import { expect, test } from '@playwright/test';

test('index page has expected a Logo', async ({ page }) => {
  await page.goto('/');
  await expect(page.getByText('LOGO')).toBeVisible();
});

test('change theme', async ({ page }) => {
  await page.goto('/');

  await expect(page.getByLabel('Light Switch')).toBeChecked({ checked: true });
  await page.getByLabel('Light Switch').click();
  await expect(page.getByLabel('Light Switch')).toBeChecked({ checked: false });
  await page.getByLabel('Light Switch').click();
  await expect(page.getByLabel('Light Switch')).toBeChecked({ checked: true });
});
