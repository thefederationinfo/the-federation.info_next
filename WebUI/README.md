# WebUI

## Technologies used

- `bun`
- `sveltekit` with `skeleton`
- `typescript`
- `houdini`
- optional `nix` to get a dev environment without installing everything on yourself

## Development

0. If you are using nix: `nix develop` (add the flag `--command <your favorite shell>` for your shell)
1. `bun install`
2. `bun dev`
3. start coding

## Building

Run `bun run build` to build the project.
Run `bun run review` to review the production build.
