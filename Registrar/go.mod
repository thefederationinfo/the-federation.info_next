module registrar

go 1.21.1

require (
	codeberg.org/thefederationinfo/nodeinfo-go v0.3.3-0.20240304130157-ad9368478bf5
	github.com/go-chi/chi/v5 v5.0.10
	github.com/go-chi/httprate v0.7.4
	github.com/go-chi/render v1.0.3
	github.com/stretchr/testify v1.8.4
	gitlab.com/golang-commonmark/puny v0.0.0-20191124015043-9f83538fa04f
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
