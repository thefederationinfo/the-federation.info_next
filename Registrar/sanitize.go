package main

import (
	"strings"

	"gitlab.com/golang-commonmark/puny"
)

// sanitize domains as it's an user input.
// we make sure it dont start or end with space chars and
// UTF8 domains with punycode.
func sanitize(in string) string {
	in = strings.TrimSpace(strings.ToLower(in))
	return puny.ToASCII(in)
}
