# Registrar

aka. GoInsert ;)

works like:

1. Check If register request comes from an IP that got ratelimited (e.g. max 5 per / hour)
  true -> 429
2. Checks If the url got already requested (aka. ratelimit via url)
  true -> 429
3. Query the NodeInfo
  error -> 500
  not_exist -> 404: return error the user can see
  found -> 202: show success page and exec 4.
4. Push URL into "NewNode"-Queue queue for QueueManager
