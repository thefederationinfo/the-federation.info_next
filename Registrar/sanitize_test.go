package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSanitize(t *testing.T) {
	assert.EqualValues(t, "xn--baw-joa.social", sanitize("BaWü.social"))
	assert.EqualValues(t, "some.test", sanitize("somE.tEst"))
	assert.EqualValues(t, "example.com", sanitize("example.com "))
}
