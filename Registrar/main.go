package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httprate"
	"github.com/go-chi/render"

	"codeberg.org/thefederationinfo/nodeinfo-go"
)

type domain struct {
	Domain string
}

func main() {
	r := chi.NewRouter()
	fetcher, err := nodeinfo.NewFetcher()
	if err != nil {
		fmt.Printf("ERROR: %v\n", err)
		return
	}

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.GetHead)

	r.Get("/ping", getPing)

	r.Route("/registrar", func(r chi.Router) {
		// ratelimit
		r.Use(httprate.LimitByIP(10, 1*time.Minute))

		r.Put("/add", func(w http.ResponseWriter, r *http.Request) {
			data, err := io.ReadAll(r.Body)
			if err != nil {
				render.Status(r, http.StatusInternalServerError)
				return
			}

			newDomain := new(domain)
			if err := json.Unmarshal(data, newDomain); err != nil {
				render.Status(r, http.StatusInternalServerError)
				return
			}

			// make sure we don't forward garbage to the crawler later
			domain := sanitize(newDomain.Domain)

			nodes, err := fetcher.QueryNode(domain)
			if err != nil {
				// TODO(6): error handling
				render.Status(r, http.StatusInternalServerError)
				render.JSON(w, r, err)
				return
			}
			// TODO(6): forward to crawler queue
			render.Status(r, http.StatusOK)
			render.JSON(w, r, nodes)
		})
	})

	http.ListenAndServe(":5000", r)
}

func getPing(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, nil)
}
