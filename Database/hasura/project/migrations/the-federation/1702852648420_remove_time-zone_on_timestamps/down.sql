ALTER TABLE public.node ALTER COLUMN created TYPE timestamptz USING created::timestamptz;
ALTER TABLE public.node ALTER COLUMN updated TYPE timestamptz USING created::timestamptz;
ALTER TABLE public.node ALTER COLUMN last_successful_crawl TYPE timestamptz USING created::timestamptz;
ALTER TABLE public.platform ALTER COLUMN created TYPE timestamptz USING created::timestamptz;
ALTER TABLE public.service ALTER COLUMN created TYPE timestamptz USING created::timestamptz;
