ALTER TABLE public.node ALTER COLUMN created TYPE timestamp USING created::timestamp;
ALTER TABLE public.node ALTER COLUMN updated TYPE timestamp USING created::timestamp;
ALTER TABLE public.node ALTER COLUMN last_successful_crawl TYPE timestamp USING created::timestamp;
ALTER TABLE public.platform ALTER COLUMN created TYPE timestamp USING created::timestamp;
ALTER TABLE public.service ALTER COLUMN created TYPE timestamp USING created::timestamp;
