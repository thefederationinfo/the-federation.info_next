-- drop old hasura data
DROP SCHEMA IF EXISTS hdb_catalog CASCADE;

-- drop not needed tables
DROP TABLE IF EXISTS public.silk_sqlquery CASCADE;
DROP TABLE IF EXISTS public.silk_response cascade;
DROP TABLE IF EXISTS public.silk_request cascade;
DROP TABLE IF EXISTS public.silk_profile_queries cascade;
DROP TABLE IF EXISTS public.silk_profile cascade;

DROP TABLE IF EXISTS public.django_session CASCADE;
DROP TABLE IF EXISTS public.django_migrations CASCADE;
DROP TABLE IF EXISTS public.django_content_type CASCADE;
DROP TABLE IF EXISTS public.django_admin_log CASCADE;

DROP TABLE IF EXISTS public.auth_user_user_permissions CASCADE;
DROP TABLE IF EXISTS public.auth_permission CASCADE;
DROP TABLE IF EXISTS public.auth_group_permissions CASCADE;
DROP TABLE IF EXISTS public.auth_user_groups CASCADE;
DROP TABLE IF EXISTS public.auth_group CASCADE;
DROP TABLE IF EXISTS public.auth_user CASCADE;

DROP TABLE IF EXISTS public.hosting_report_t CASCADE;
DROP TABLE IF EXISTS public.count_by_date CASCADE;

-- drop not needed columns
ALTER TABLE public.thefederation_protocol DROP CONSTRAINT IF EXISTS thefederation_protocol_uuid_key;
ALTER TABLE public.thefederation_protocol DROP COLUMN IF EXISTS uuid;
ALTER TABLE public.thefederation_protocol DROP COLUMN IF EXISTS updated;
ALTER TABLE public.thefederation_protocol DROP COLUMN IF EXISTS website;
ALTER TABLE public.thefederation_protocol DROP COLUMN IF EXISTS display_name;
ALTER TABLE public.thefederation_protocol DROP COLUMN IF EXISTS description;

ALTER TABLE public.thefederation_node DROP CONSTRAINT IF EXISTS thefederation_node_uuid_key;
ALTER TABLE public.thefederation_node DROP COLUMN IF EXISTS uuid;
ALTER TABLE public.thefederation_node DROP COLUMN IF EXISTS server_meta;
ALTER TABLE public.thefederation_node DROP COLUMN IF EXISTS relay;

ALTER TABLE public.thefederation_platform DROP CONSTRAINT IF EXISTS thefederation_platform_uuid_key;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS uuid;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS icon;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS latest_version;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS version_clean_style;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS license;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS install_guide;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS description;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS tagline;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS display_name;
ALTER TABLE public.thefederation_platform DROP COLUMN IF EXISTS updated;

ALTER TABLE public.thefederation_service DROP CONSTRAINT IF EXISTS thefederation_service_uuid_key;
ALTER TABLE public.thefederation_service DROP COLUMN IF EXISTS uuid;
ALTER TABLE public.thefederation_service DROP COLUMN IF EXISTS updated;

-- drop calculated stats
ALTER TABLE public.thefederation_stat DROP CONSTRAINT IF EXISTS thefederation_stat_date_platform_id_8041fdd3_uniq;
ALTER TABLE public.thefederation_stat DROP CONSTRAINT IF EXISTS thefederation_stat_platform_id_6aa40433_fk_thefedera;
ALTER TABLE public.thefederation_stat DROP COLUMN IF EXISTS platform_id;
ALTER TABLE public.thefederation_stat DROP CONSTRAINT IF EXISTS thefederation_stat_protocol_id_ead14167_fk_thefedera;
ALTER TABLE public.thefederation_stat DROP CONSTRAINT IF EXISTS thefederation_stat_date_protocol_id_d73e895d_uniq;
ALTER TABLE public.thefederation_stat DROP COLUMN IF EXISTS protocol_id;

DELETE FROM public.thefederation_stat WHERE public.thefederation_stat.node_id IS NULL;

-- clean records
DELETE FROM public.thefederation_protocol WHERE public.thefederation_protocol.id NOT IN (
  SELECT public.thefederation_node_protocols.protocol_id FROM public.thefederation_node_protocols);

DELETE FROM public.thefederation_platform WHERE public.thefederation_platform.id NOT IN (
  SELECT public.thefederation_node.platform_id FROM public.thefederation_node);

-- migrate to new schema
-- 1. rename columns
ALTER TABLE IF EXISTS public.thefederation_platform RENAME COLUMN code TO repository;
ALTER TABLE IF EXISTS public.thefederation_platform RENAME COLUMN website TO homepage;
ALTER TABLE IF EXISTS public.thefederation_stat RENAME COLUMN "date" TO "timestamp";
ALTER TABLE IF EXISTS public.thefederation_node RENAME COLUMN last_success TO last_successful_crawl;
ALTER TABLE IF EXISTS public.thefederation_node RENAME COLUMN hide_from_list TO hide;
ALTER TABLE IF EXISTS public.thefederation_node RENAME COLUMN host TO "domain";
ALTER TABLE IF EXISTS public.thefederation_node RENAME COLUMN "name" TO "full_name";
ALTER TABLE IF EXISTS public.thefederation_node ALTER COLUMN full_name TYPE varchar(500) USING full_name::varchar(500);

-- 2. rename tables
ALTER TABLE IF EXISTS public.thefederation_platform RENAME TO platform;
ALTER TABLE IF EXISTS public.thefederation_node RENAME TO node;
ALTER TABLE IF EXISTS public.thefederation_service RENAME TO service;
ALTER TABLE IF EXISTS public.thefederation_stat RENAME TO stat;

-- add columns
ALTER TABLE public.stat ADD inbound_services jsonb NULL;
ALTER TABLE public.stat ADD outbound_services jsonb NULL;

-- 3. migrate old data to new tables

-- 3.1 node-service
CREATE TABLE IF NOT EXISTS public.service_inbound (
  service_id serial4 NOT NULL,
  node_id serial4 NOT NULL,
  CONSTRAINT service_inbound_pk PRIMARY KEY (service_id,node_id),
  CONSTRAINT service_inbound_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT service_inbound_service_fk FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS public.service_outbound (
  service_id serial4 NOT NULL,
  node_id serial4 NOT NULL,
  CONSTRAINT service_outbound_pk PRIMARY KEY (service_id,node_id),
  CONSTRAINT service_outbound_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT service_outbound_service_fk FOREIGN KEY (service_id) REFERENCES public.service(id) ON DELETE CASCADE ON UPDATE CASCADE
);
-- we duplicate it for now, the new crawler will update and fix there values for new or still running nodes
INSERT INTO public.service_inbound (SELECT service_id,node_id FROM public.thefederation_node_services);
INSERT INTO public.service_outbound (SELECT service_id,node_id FROM public.thefederation_node_services);
DROP TABLE public.thefederation_node_services;

-- 3.2 protocols
-- we break normalization to gain in query speed
CREATE TABLE IF NOT EXISTS public.protocol (
  name varchar NOT NULL,
  node_id serial4 NOT NULL,
  CONSTRAINT protocol_pk PRIMARY KEY (name,node_id),
  CONSTRAINT protocol_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO public.protocol(
  SELECT thefederation_protocol.name AS name,thefederation_node_protocols.node_id AS node_id FROM public.thefederation_node_protocols
  INNER JOIN public.thefederation_protocol ON thefederation_node_protocols.protocol_id=thefederation_protocol.id
  ORDER BY thefederation_node_protocols.ID
);

DROP TABLE thefederation_node_protocols;
DROP TABLE thefederation_protocol;

-- 3.3 stat
ALTER TABLE public.stat ALTER COLUMN "timestamp" TYPE timestamp USING "timestamp"::timestamp;

-- 4. node migrations

CREATE TABLE IF NOT EXISTS public.node_info (
  node_id serial4 NOT NULL,
  "timestamp" timestamp NOT NULL,
  "data" jsonb NOT NULL,
  CONSTRAINT node_info_pk PRIMARY KEY (node_id,"timestamp"),
  CONSTRAINT node_info_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- just make sure it is never is null
UPDATE public.node set features = '{}'::jsonb WHERE node.features IS NULL;

-- wrap stored features into metadata object
UPDATE public.node set features = jsonb_set('{}'::jsonb, '{metadata}', features) WHERE features != '{}'::jsonb;
-- create empty metadata if no features where set but we have organization info to store
UPDATE public.node set features = '{"metadata":{}}'::jsonb WHERE features = '{}'::jsonb AND (organization_account NOT LIKE '' OR organization_contact NOT LIKE '' OR organization_name NOT LIKE '');

UPDATE public.node set features = jsonb_set(features, '{metadata}', '{}'::jsonb)
  WHERE node.features->'metadata' IS NULL
  AND node.full_name NOT LIKE node.domain
  AND node.full_name NOT LIKE '';
UPDATE public.node set features = jsonb_set(features, '{metadata, nodeName}', to_jsonb(node.full_name))
  WHERE node.features->'metadata'->'nodeName' IS NULL
  AND node.full_name NOT LIKE node.domain
  AND node.full_name NOT LIKE '';

-- use a tmp column
ALTER TABLE public.node ADD tmp jsonb NULL;
WITH aggregated_data AS (
  SELECT
      id,
      jsonb_agg(
          jsonb_build_object(
              'name', organization_name,
              'account', organization_account,
              'email', organization_contact
          )
      ) AS admin_accounts
  FROM public.node
  WHERE
      organization_account NOT LIKE '' OR
      organization_contact NOT LIKE '' OR
      organization_name NOT LIKE ''
  GROUP BY id
)
UPDATE public.node SET tmp = admin_accounts
FROM aggregated_data WHERE public.node.id = aggregated_data.id;

-- integrate data from tmp column
UPDATE public.node SET features = jsonb_set(features, '{metadata, adminAccounts}', tmp)
  WHERE
    tmp IS NOT NULL AND
    jsonb_typeof(features->'metadata') != 'array' AND
    features->'metadata'->'adminAccounts' IS NULL;

INSERT INTO public.node_info (
  SELECT id AS node_id,
  last_successful_crawl AS "timestamp",
  features AS data
  FROM public.node WHERE features != '{}'::jsonb
);

ALTER TABLE public.node DROP COLUMN features;
ALTER TABLE public.node DROP COLUMN organization_account;
ALTER TABLE public.node DROP COLUMN organization_contact;
ALTER TABLE public.node DROP COLUMN organization_name;
ALTER TABLE public.node DROP COLUMN tmp;

-- 5. manage indices
CREATE INDEX protocol_name_idx ON public.protocol (name);
CREATE INDEX protocol_node_id_idx ON public.protocol (node_id);
-- deduplicate some
DROP INDEX IF EXISTS public.thefederation_service_name_72b5a7b2_like;
DROP INDEX public.thefederation_platform_name_533bcf40_like;

DROP INDEX IF EXISTS public.thefederation_stat_node_id_6cabc5ae;
DROP INDEX IF EXISTS public.thefederation_stat_date_8546a77b;
CREATE INDEX stat_timestamp_idx ON public.stat ("timestamp");
CREATE INDEX stat_node_id_idx ON public.stat (node_id);

ALTER TABLE public.stat DROP CONSTRAINT IF EXISTS thefederation_stat_date_node_id_b82269f9_uniq;
ALTER TABLE public.stat DROP CONSTRAINT IF EXISTS thefederation_stat_node_id_6cabc5ae_fk_thefederation_node_id;
ALTER TABLE public.stat ADD CONSTRAINT stat_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE INDEX node_info_timestamp_idx ON public.node_info ("timestamp");
CREATE INDEX node_info_node_id_idx ON public.node_info (node_id);

DROP INDEX IF EXISTS public.thefederation_node_host_3c592362_like;
DROP INDEX IF EXISTS public.thefederation_node_last_success_29deed21;
DROP INDEX IF EXISTS public.thefederation_node_platform_id_029302aa;

CREATE INDEX node_country_idx ON public.node (country);
CREATE INDEX node_platform_id_idx ON public.node (platform_id);
CREATE INDEX node_last_successful_crawl_idx ON public.node (last_successful_crawl);

-- 6. add constrains
UPDATE public.node SET hide = true WHERE blocked IS true;
ALTER TABLE public.node ADD CONSTRAINT node_hide_check CHECK (NOT (blocked AND NOT hide));

-- 7. add more indices to speed up queries
CREATE INDEX node_hide_idx ON public.node (hide);
CREATE INDEX node_blocked_idx ON public.node ("blocked");
CREATE INDEX node_created_idx ON public.node (created);
CREATE INDEX node_version_idx ON public.node ("version");
CREATE INDEX node_version_platform_idx ON public.node ("version",platform_id);

CREATE INDEX service_inbound_service_id_idx ON public.service_inbound (service_id);
CREATE INDEX service_inbound_node_id_idx ON public.service_inbound (node_id);

CREATE INDEX service_outbound_service_id_idx ON public.service_outbound (service_id);
CREATE INDEX service_outbound_node_id_idx ON public.service_outbound (node_id);

CREATE INDEX stat_local_posts_idx ON public.stat (local_posts);
CREATE INDEX stat_local_comments_idx ON public.stat (local_comments);
CREATE INDEX stat_users_weekly_idx ON public.stat (users_weekly);
CREATE INDEX stat_users_monthly_idx ON public.stat (users_monthly);
CREATE INDEX stat_users_half_year_idx ON public.stat (users_half_year);
CREATE INDEX stat_users_total_idx ON public.stat (users_total);

-- 8. fix broken data caused by old crawler code
UPDATE public.platform SET name = REPLACE(LOWER(name), ' ', '_');
UPDATE public.platform SET name = REPLACE(name, 'matrix|', '') WHERE name like 'matrix|%';
