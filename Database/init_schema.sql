--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1 (Debian 16.1-1.pgdg120+1)
-- Dumped by pg_dump version 16.1 (Debian 16.1-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: node; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.node (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    blocked boolean NOT NULL,
    country character varying(2) NOT NULL,
    hide boolean NOT NULL,
    domain character varying(128) NOT NULL,
    ip inet,
    full_name character varying(500) NOT NULL,
    open_signups boolean NOT NULL,
    version character varying(128) NOT NULL,
    platform_id integer NOT NULL,
    last_successful_crawl timestamp with time zone,
    CONSTRAINT node_hide_check CHECK ((NOT (blocked AND (NOT hide))))
);


ALTER TABLE public.node OWNER TO thefederation;

--
-- Name: node_info; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.node_info (
    node_id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    data jsonb NOT NULL
);


ALTER TABLE public.node_info OWNER TO thefederation;

--
-- Name: node_info_node_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.node_info_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.node_info_node_id_seq OWNER TO thefederation;

--
-- Name: node_info_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.node_info_node_id_seq OWNED BY public.node_info.node_id;


--
-- Name: platform; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.platform (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL,
    repository character varying(128) NOT NULL,
    homepage character varying(128) NOT NULL
);


ALTER TABLE public.platform OWNER TO thefederation;

--
-- Name: protocol; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.protocol (
    name character varying NOT NULL,
    node_id integer NOT NULL
);


ALTER TABLE public.protocol OWNER TO thefederation;

--
-- Name: protocol_node_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.protocol_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.protocol_node_id_seq OWNER TO thefederation;

--
-- Name: protocol_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.protocol_node_id_seq OWNED BY public.protocol.node_id;


--
-- Name: service; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.service (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.service OWNER TO thefederation;

--
-- Name: service_inbound; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.service_inbound (
    service_id integer NOT NULL,
    node_id integer NOT NULL
);


ALTER TABLE public.service_inbound OWNER TO thefederation;

--
-- Name: service_inbound_node_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.service_inbound_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_inbound_node_id_seq OWNER TO thefederation;

--
-- Name: service_inbound_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.service_inbound_node_id_seq OWNED BY public.service_inbound.node_id;


--
-- Name: service_inbound_service_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.service_inbound_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_inbound_service_id_seq OWNER TO thefederation;

--
-- Name: service_inbound_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.service_inbound_service_id_seq OWNED BY public.service_inbound.service_id;


--
-- Name: service_outbound; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.service_outbound (
    service_id integer NOT NULL,
    node_id integer NOT NULL
);


ALTER TABLE public.service_outbound OWNER TO thefederation;

--
-- Name: service_outbound_node_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.service_outbound_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_outbound_node_id_seq OWNER TO thefederation;

--
-- Name: service_outbound_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.service_outbound_node_id_seq OWNED BY public.service_outbound.node_id;


--
-- Name: service_outbound_service_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.service_outbound_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.service_outbound_service_id_seq OWNER TO thefederation;

--
-- Name: service_outbound_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.service_outbound_service_id_seq OWNED BY public.service_outbound.service_id;


--
-- Name: stat; Type: TABLE; Schema: public; Owner: thefederation
--

CREATE TABLE public.stat (
    id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    users_total integer,
    users_half_year integer,
    users_monthly integer,
    users_weekly integer,
    local_posts integer,
    local_comments integer,
    node_id integer,
    inbound_services jsonb,
    outbound_services jsonb,
    CONSTRAINT thefederation_stat_local_comments_check CHECK ((local_comments >= 0)),
    CONSTRAINT thefederation_stat_local_posts_check CHECK ((local_posts >= 0)),
    CONSTRAINT thefederation_stat_users_half_year_check CHECK ((users_half_year >= 0)),
    CONSTRAINT thefederation_stat_users_monthly_check CHECK ((users_monthly >= 0)),
    CONSTRAINT thefederation_stat_users_total_check CHECK ((users_total >= 0)),
    CONSTRAINT thefederation_stat_users_weekly_check CHECK ((users_weekly >= 0))
);


ALTER TABLE public.stat OWNER TO thefederation;

--
-- Name: thefederation_node_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.thefederation_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.thefederation_node_id_seq OWNER TO thefederation;

--
-- Name: thefederation_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.thefederation_node_id_seq OWNED BY public.node.id;


--
-- Name: thefederation_platform_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.thefederation_platform_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.thefederation_platform_id_seq OWNER TO thefederation;

--
-- Name: thefederation_platform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.thefederation_platform_id_seq OWNED BY public.platform.id;


--
-- Name: thefederation_service_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.thefederation_service_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.thefederation_service_id_seq OWNER TO thefederation;

--
-- Name: thefederation_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.thefederation_service_id_seq OWNED BY public.service.id;


--
-- Name: thefederation_stat_id_seq; Type: SEQUENCE; Schema: public; Owner: thefederation
--

CREATE SEQUENCE public.thefederation_stat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.thefederation_stat_id_seq OWNER TO thefederation;

--
-- Name: thefederation_stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: thefederation
--

ALTER SEQUENCE public.thefederation_stat_id_seq OWNED BY public.stat.id;


--
-- Name: node id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node ALTER COLUMN id SET DEFAULT nextval('public.thefederation_node_id_seq'::regclass);


--
-- Name: node_info node_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node_info ALTER COLUMN node_id SET DEFAULT nextval('public.node_info_node_id_seq'::regclass);


--
-- Name: platform id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.platform ALTER COLUMN id SET DEFAULT nextval('public.thefederation_platform_id_seq'::regclass);


--
-- Name: protocol node_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.protocol ALTER COLUMN node_id SET DEFAULT nextval('public.protocol_node_id_seq'::regclass);


--
-- Name: service id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service ALTER COLUMN id SET DEFAULT nextval('public.thefederation_service_id_seq'::regclass);


--
-- Name: service_inbound service_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_inbound ALTER COLUMN service_id SET DEFAULT nextval('public.service_inbound_service_id_seq'::regclass);


--
-- Name: service_inbound node_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_inbound ALTER COLUMN node_id SET DEFAULT nextval('public.service_inbound_node_id_seq'::regclass);


--
-- Name: service_outbound service_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_outbound ALTER COLUMN service_id SET DEFAULT nextval('public.service_outbound_service_id_seq'::regclass);


--
-- Name: service_outbound node_id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_outbound ALTER COLUMN node_id SET DEFAULT nextval('public.service_outbound_node_id_seq'::regclass);


--
-- Name: stat id; Type: DEFAULT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.stat ALTER COLUMN id SET DEFAULT nextval('public.thefederation_stat_id_seq'::regclass);


--
-- Name: node_info node_info_pk; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node_info
    ADD CONSTRAINT node_info_pk PRIMARY KEY (node_id, "timestamp");


--
-- Name: protocol protocol_pk; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.protocol
    ADD CONSTRAINT protocol_pk PRIMARY KEY (name, node_id);


--
-- Name: service_inbound service_inbound_pk; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_inbound
    ADD CONSTRAINT service_inbound_pk PRIMARY KEY (service_id, node_id);


--
-- Name: service_outbound service_outbound_pk; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_outbound
    ADD CONSTRAINT service_outbound_pk PRIMARY KEY (service_id, node_id);


--
-- Name: node thefederation_node_host_key; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT thefederation_node_host_key UNIQUE (domain);


--
-- Name: node thefederation_node_pkey; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT thefederation_node_pkey PRIMARY KEY (id);


--
-- Name: platform thefederation_platform_name_key; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT thefederation_platform_name_key UNIQUE (name);


--
-- Name: platform thefederation_platform_pkey; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT thefederation_platform_pkey PRIMARY KEY (id);


--
-- Name: service thefederation_service_name_key; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT thefederation_service_name_key UNIQUE (name);


--
-- Name: service thefederation_service_pkey; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT thefederation_service_pkey PRIMARY KEY (id);


--
-- Name: stat thefederation_stat_pkey; Type: CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT thefederation_stat_pkey PRIMARY KEY (id);


--
-- Name: node_country_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX node_country_idx ON public.node USING btree (country);


--
-- Name: node_info_node_id_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX node_info_node_id_idx ON public.node_info USING btree (node_id);


--
-- Name: node_info_timestamp_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX node_info_timestamp_idx ON public.node_info USING btree ("timestamp");


--
-- Name: node_last_successful_crawl_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX node_last_successful_crawl_idx ON public.node USING btree (last_successful_crawl);


--
-- Name: node_platform_id_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX node_platform_id_idx ON public.node USING btree (platform_id);


--
-- Name: protocol_name_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX protocol_name_idx ON public.protocol USING btree (name);


--
-- Name: protocol_node_id_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX protocol_node_id_idx ON public.protocol USING btree (node_id);


--
-- Name: stat_node_id_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX stat_node_id_idx ON public.stat USING btree (node_id);


--
-- Name: stat_timestamp_idx; Type: INDEX; Schema: public; Owner: thefederation
--

CREATE INDEX stat_timestamp_idx ON public.stat USING btree ("timestamp");


--
-- Name: node_info node_info_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node_info
    ADD CONSTRAINT node_info_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: protocol protocol_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.protocol
    ADD CONSTRAINT protocol_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: service_inbound service_inbound_node_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_inbound
    ADD CONSTRAINT service_inbound_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: service_inbound service_inbound_service_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_inbound
    ADD CONSTRAINT service_inbound_service_fk FOREIGN KEY (service_id) REFERENCES public.service(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: service_outbound service_outbound_node_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_outbound
    ADD CONSTRAINT service_outbound_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: service_outbound service_outbound_service_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.service_outbound
    ADD CONSTRAINT service_outbound_service_fk FOREIGN KEY (service_id) REFERENCES public.service(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stat stat_node_fk; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT stat_node_fk FOREIGN KEY (node_id) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node thefederation_node_platform_id_029302aa_fk_thefedera; Type: FK CONSTRAINT; Schema: public; Owner: thefederation
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT thefederation_node_platform_id_029302aa_fk_thefedera FOREIGN KEY (platform_id) REFERENCES public.platform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

