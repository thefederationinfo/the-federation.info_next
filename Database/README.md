# Database

> Click [here](#hasura-migrations) for database migrations!

## Info

The Platform table is overloaded and extended by [Metadata's json file](../Metadata/README.md)

## Developer

The schema is not completely normalized, to gain speed in join operations and to make it easy for WebUI to access data.

## ER

```mermaid
erDiagram
  Platform ||--o{ Node : is
  Node ||--o{ Stat: has
  Node ||--o{ Protocol : implements
  Service }o--o{ Node: Inbound
  Service }o--o{ Node: Outbound
  Node ||--o{ NodeInfo: had

  Platform {
    int       id    PK
    string    name        "UNIQUE lowercase (PK für json)"
    string    homepage    "NULLABLE"
    string    repository  "NULLABLE"
    timestamp created
  }

  Node {
    int       id PK
    int       platform_id FK
    string    domain
    string    full_name    "NULLABLE"
    bool      abandoned    "Default=false; no new crawl"
    bool      hide         "Default=false; hide in WebUI"
    bool      blocked      "Default=false; no new crawl & hide in WebUI"
    bool      proprietary  "Default=false"
    string    version      "NULLABLE"
    string    country      "NULLABLE"
    bool      open_signups "Default=false"
    timestamp last_successful_crawl
    timestamp created
    inet      ip
    timestamp updated      "used to note that the crawler did touched it"
  }

  %% ServiceInbound { # n-m help table (tuple UNIQUES)
  %%   int service_id
  %%   int node_id
  %% }

  %% ServiceOutbound { # n-m help table (tuple UNIQUES)
  %%   int service_id
  %%   int node_id
  %% }

  Service {
    int id PK
    string name "UNIQUE e.g. 'imap'"
    timestamp created
  }

  Protocol {
    string name PK "e.g. 'diaspora'"
    int node_id PK,FK
  }

  Stat {
    int       id PK
    int       node_id FK
    timestamp timestamp
    int       users_total       "NULLABLE"
    int       users_monthly     "NULLABLE"
    int       users_half_year   "NULLABLE"
    int       users_weekly      "NULLABLE"
    int       local_posts       "NULLABLE"
    int       local_comments    "NULLABLE"
    json      inbound_services  "NULLABLE"
    json      outbound_services "NULLABLE"
  }

  NodeInfo {
    int       node_id   PK,FK
    timestamp timestamp PK
    json      data          "raw nodeinfo"
  }
```

## Hasura Migrations

Table migrations **ONLY** over Hasura.
To create a Hasura Migration you have to [install the Hasura-CLI](https://hasura.io/docs/latest/hasura-cli/install-hasura-cli/).
If you have Nix you can use the `flake.nix` instead.

After that navigate into `hasura/project`. Type the following:

```bash
hasura console --admin-secret "<your-secret-key-for-the-admin-console>"
```

A browser should open.
Every database modification will be tracked as migration.
If you have to write a SQL-Migration navigate to `Data > SQL` and tick "This is a migration".
