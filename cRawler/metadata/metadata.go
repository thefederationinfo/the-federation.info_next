package metadata

import (
	_ "embed"
	"encoding/json"
)

//go:generate cp ../../Metadata/platform_metadata.json platform_metadata.json
//go:embed platform_metadata.json
var platform_metadata []byte

var loadedMetadata map[string]platformMetadata

type platformMetadata struct {
	Name        string `json:"name"`
	Proprietary bool   `json:"proprietary"`
}

func init() {
	loadedMetadata = make(map[string]platformMetadata)
	pml := make([]platformMetadata, 0, 32)
	if err := json.Unmarshal(platform_metadata, &pml); err != nil {
		panic(err)
	}
	for _, p := range pml {
		loadedMetadata[p.Name] = p
	}
}

func LookupProprietary(platform string) bool {
	meta, found := loadedMetadata[platform]
	if !found {
		return false
	}
	return meta.Proprietary
}
