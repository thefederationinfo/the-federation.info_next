package cron

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"

	"crawler/database"
	"crawler/queue"
)

type Config struct {
	// CheckTime specifies the interval to add new batch into queue
	CheckTime time.Duration
	// checkItems specifies the batch size of items
	CheckItems int
}

var DefaultConfig = Config{
	CheckTime:  5 * time.Minute,
	CheckItems: 1000,
}

func Start(ctx context.Context, db database.Database, q queue.Queue, conf *Config) error {
	if conf == nil {
		conf = &DefaultConfig
	}
	log.Info().Msg("start cron service")
	log := log.With().Str("component", "cron").Logger()

	for {
		select {
		case <-ctx.Done():
			return nil
		case <-time.After(conf.CheckTime):
			go func() {
				domains, err := db.GetCrawlList(conf.CheckItems)
				if err != nil {
					log.Error().Err(err).Msg("db GetCrawlList")
					return
				}

				if len(domains) == 0 {
					log.Info().Msg("Database return empty list to crawl. We are done for today 🎉!")
				}

				for _, domain := range domains {
					err := q.Add(&queue.Task{Domain: domain})
					if err != nil {
						log.Error().Err(err).Msgf("queue Add '%s'", domain)
					}
				}
			}()
		}
	}
}
