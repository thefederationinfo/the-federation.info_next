package fetch

import (
	"context"
)

type Fetcher interface {
	Start(context.Context) error
}
