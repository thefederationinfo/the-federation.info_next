package fetch

import (
	"sync"
	"time"

	"crawler/database"
)

var (
	failedRetrys sync.Map

	allowedFails         = 3
	lastAllowedCrawlFail = 7 * 24 * time.Hour
)

func shouldAbandoned(node *database.Node) bool {
	if time.Time(node.LastSuccessfulCrawl).Add(lastAllowedCrawlFail).Compare(time.Now()) != -1 {
		return false
	}
	val, ok := failedRetrys.Load(node.Domain)
	if !ok {
		failedRetrys.Store(node.Domain, int(1))
		return false
	}
	count, _ := val.(int)
	count++
	if count <= allowedFails {
		failedRetrys.Store(node.Domain, count)
		return false
	}

	failedRetrys.Delete(node.Domain)
	return true
}
