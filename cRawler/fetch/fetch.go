package fetch

import (
	"context"
	"time"

	"crawler/database"
	"crawler/queue"

	"codeberg.org/thefederationinfo/nodeinfo-go"
	"github.com/rs/zerolog/log"
	"golang.org/x/sync/errgroup"

	// enable extending fetcher for generic fixes, mastodon and socialhome
	_ "codeberg.org/thefederationinfo/nodeinfo-go/extension"
)

type fetcher struct {
	internal nodeinfo.Fetcher
	db       database.Database
	queue    queue.Queue
	worker   int
}

func New(db database.Database, q queue.Queue, worker int) Fetcher {
	f, err := nodeinfo.NewFetcher()
	if err != nil {
		log.Fatal().Msg("create new internal nodeinfo fetcher")
	}
	return &fetcher{
		internal: f,
		db:       db,
		queue:    q,
		worker:   worker,
	}
}

func (f *fetcher) Start(ctx context.Context) error {
	log := log.With().Str("component", "fetch").Logger()
	log.Info().Msg("start fetcher service")

	if f.worker <= 0 {
		log.Warn().Msgf("fetching is disabled as CRAWLER_WORKERS is %d", f.worker)
		return nil
	}

	g, ctx := errgroup.WithContext(ctx)
	for i := 1; i < f.worker; i++ {
		g.Go(func() error {
			return f.run(ctx)
		})
	}

	return g.Wait()
}

func (f *fetcher) run(ctx context.Context) error {
	log := log.With().Str("component", "fetch").Logger()
	for {
		select {
		case <-ctx.Done():
			return nil
		case <-time.After(10 * time.Millisecond):
			t, err := f.queue.Get()
			if err != nil {
				log.Error().Err(err).Msg("get task from queue")
				continue
			}
			if t == nil {
				continue
			}
			log.Debug().Msgf("got task '%s' from queue", t.Domain)
			f.runTask(t)
		}
	}
}

func (f *fetcher) runTask(t *queue.Task) {
	log := log.With().Str("component", "fetch").Logger()
	now := time.Now().UTC()

	dbNode, err := f.db.GetNode(t.Domain)
	if err != nil {
		log.Error().Err(err).Msgf("get node '%s' from db", t.Domain)
		return
	}

	if dbNode.Blocked || dbNode.Abandoned {
		log.Info().Msgf("node '%s' is blocked from fetching", t.Domain)
		return
	}

	nodes, err := f.internal.QueryNode(t.Domain)
	if err != nil {
		log.Error().Err(err).Msgf("fetch nodeinfo from '%s'", t.Domain)

		if shouldAbandoned(dbNode) {
			f.db.Abandoned(t.Domain)
		}

		return
	}

	if len(nodes) == 0 {
		return
	}
	node := nodes[0]
	if len(nodes) != 1 {
		log.Debug().Msg("got more than one node, try to select non matrix one")
		// well the go lib allow to discover more software ... but we support one per domain atm.
		// so the case can be if an matrix server is running on the same domain as some other federated software
		// in this case we just ignore matrix (e.g. "diasp.in")
		for i := range nodes {
			if nodes[i] != nil && (len(nodes[i].Protocols) != 1 || nodes[i].Protocols[0] != "matrix") {
				node = nodes[i]
			}
		}
	}

	log.Debug().Interface("node", node).Msg("try to insert into db")
	f.db.Insert(
		node,
		t.Domain,
		database.Timestamp(now),
	)
}
