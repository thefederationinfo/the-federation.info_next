package api

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"crawler/queue"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/rs/zerolog/log"
)

func Serve(ctx context.Context, q queue.Queue) error {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.GetHead)

	r.Get("/ping", getPing)
	r.Route("/crawler", func(r chi.Router) {
		r.Put("/add", addToQueue(q))
	})

	server := &http.Server{Addr: ":6000", Handler: r}

	// shutdown server gracefully if context is canceled
	go func() {
		<-ctx.Done()
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		server.Shutdown(ctx)
	}()

	log.Info().Msg("Start http server")
	return server.ListenAndServe()
}

func addToQueue(q queue.Queue) func(w http.ResponseWriter, r *http.Request) {
	log := log.With().Str("component", "api").Logger()
	return func(w http.ResponseWriter, r *http.Request) {
		type domain struct {
			Domain string `json:"domain"`
		}

		data, err := io.ReadAll(r.Body)
		if err != nil {
			render.Status(r, http.StatusInternalServerError)
			return
		}

		newDomain := new(domain)
		if err := json.Unmarshal(data, newDomain); err != nil {
			log.Error().Err(err).Msg("addToQueue: could not unmarshal")
			render.Status(r, http.StatusInternalServerError)
			return
		}

		log.Debug().Msgf("add task for '%s' into queue", newDomain.Domain)
		q.AddPrio(&queue.Task{Domain: newDomain.Domain})

		render.Status(r, http.StatusOK)
		render.JSON(w, r, map[string]string{"queue": "added"})
	}
}

func getPing(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"status": "ok"})
}
