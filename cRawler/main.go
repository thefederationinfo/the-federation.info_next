package main

import (
	"context"
	"os"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/sync/errgroup"

	"crawler/api"
	"crawler/cron"
	"crawler/database"
	"crawler/fetch"
	"crawler/queue"
)

var version string = "dev"

func main() {
	setupLogger()

	g, ctx := errgroup.WithContext(context.Background())
	q := queue.New()
	db, err := database.New(os.Getenv("CRAWLER_DATABASE_URL"))
	if err != nil {
		log.Fatal().Err(err).Msg("create database connection")
	}
	workers, _ := strconv.Atoi(os.Getenv("CRAWLER_WORKERS"))
	f := fetch.New(db, q, workers)

	// start server
	g.Go(func() error {
		return api.Serve(ctx, q)
	})

	// start cron
	g.Go(func() error {
		return cron.Start(ctx, db, q, nil)
	})

	// start fetcher
	g.Go(func() error {
		return f.Start(ctx)
	})

	g.Wait()
}

func setupLogger() {
	zerolog.SetGlobalLevel(zerolog.WarnLevel)
	logLevel := os.Getenv("CRAWLER_LOG_LEVEL")
	if logLevel != "" {
		if lvl, err := zerolog.ParseLevel(logLevel); err == nil {
			zerolog.SetGlobalLevel(lvl)
		}
	}

	log.Logger = log.Output(
		zerolog.ConsoleWriter{
			Out:     os.Stdout,
			NoColor: false,
		},
	)
	// if debug or trace also log the caller
	if zerolog.GlobalLevel() <= zerolog.DebugLevel {
		log.Logger = log.With().Caller().Logger()
	}

	log.Info().Msgf("start cRawler with version %s", version)
}
