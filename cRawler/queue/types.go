package queue

type Task struct {
	Domain string
}

type Queue interface {
	Add(*Task) error
	Get() (*Task, error)
	AddPrio(*Task) error
	Reset() error
	Length() int
	Close() error
}
