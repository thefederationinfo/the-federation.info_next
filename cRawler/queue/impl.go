package queue

import (
	"sync"

	"github.com/gammazero/deque"
)

type queue struct {
	internal *deque.Deque[*Task]
	lock     sync.Mutex
}

func New() Queue {
	return &queue{
		internal: deque.New[*Task](),
		lock:     sync.Mutex{},
	}
}

func (q *queue) Add(t *Task) error {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.internal.PushBack(t)
	return nil
}

func (q *queue) AddPrio(t *Task) error {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.internal.PushFront(t)
	return nil
}

func (q *queue) Get() (*Task, error) {
	q.lock.Lock()
	defer q.lock.Unlock()
	if q.internal.Len() == 0 {
		return nil, nil
	}
	return q.internal.PopFront(), nil
}

func (q *queue) Reset() error {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.internal.Clear()
	return nil
}

func (q *queue) Length() int {
	q.lock.Lock()
	defer q.lock.Unlock()
	return q.internal.Len()
}

func (q *queue) Close() error {
	q.Reset()
	q = nil
	return nil
}
