package database

import (
	"net"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"

	"crawler/metadata"

	nodeinfo "codeberg.org/thefederationinfo/nodeinfo-go"
	known_extension "codeberg.org/thefederationinfo/nodeinfo_metadata_survey"
	"github.com/ip2location/ip2location-go"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"xorm.io/builder"
	"xorm.io/xorm"
	xlog "xorm.io/xorm/log"
)

type database struct {
	e *xorm.Engine
}

func New(conn string) (Database, error) {
	e, err := xorm.NewEngine("postgres", conn)
	if err != nil {
		return nil, err
	}
	logSQL, _ := strconv.ParseBool(os.Getenv("CRAWLER_LOG_SQL"))
	e.ShowSQL(logSQL)
	e.SetLogger(newXORMLogger(xlog.LogLevel(zerolog.GlobalLevel())))
	if err := e.Ping(); err != nil {
		return nil, err
	}
	return &database{e: e}, nil
}

func (db *database) GetCrawlList(limit int) ([]string, error) {
	log := log.With().Str("component", "database").Logger()
	sess := db.e.NewSession()
	if err := sess.Begin(); err != nil {
		log.Error().Err(err).Msg("can not start db session")
		return nil, err
	}
	defer sess.Close()

	today := time.Now().UTC().Format(time.DateOnly)
	var domains []string

	cond := builder.NewCond().
		And(builder.Eq{"blocked": false, "abandoned": false}.
			And(builder.Lt{"last_successful_crawl": today}))

	if err := sess.Table(new(Node)).
		Where(cond).
		Select("domain").
		OrderBy("updated").
		Limit(limit).
		Find(&domains); err != nil {
		log.Error().Err(err).Msg("get string list of domains failed")
		return nil, err
	}

	if _, err := sess.Where(cond).
		Cols("updated").
		Update(&Node{Updated: Timestamp(time.Now().UTC())}); err != nil {
		log.Error().Err(err).Msg("could not updated list of domains fetched")
		return nil, err
	}

	if err := sess.Commit(); err != nil {
		log.Error().Err(err).Msg("can not commit db session")
		return nil, err
	}
	log.Trace().Msg("db session successfully committed")
	return domains, nil
}

func (db *database) GetNode(domain string) (*Node, error) {
	node := new(Node)
	_, err := db.e.Where("domain = ?", domain).Get(node)
	return node, err
}

func (db *database) Abandoned(domain string) {
	log := log.With().Str("component", "database").Str("domain", domain).Logger()
	node, err := db.GetNode(domain)
	if err != nil {
		log.Error().Err(err).Msg("could not aboanded node")
	}
	node.Abandoned = true
	_ = updateNode(log, db.e.NewSession(), node)
}

func (db *database) Insert(nodeInfo *nodeinfo.Node, domain string, crawled Timestamp) {
	log := log.With().Str("component", "database").Str("domain", domain).Logger()
	sess := db.e.NewSession()
	if err := sess.Begin(); err != nil {
		log.Error().Err(err).Msg("can not start db session")
		return
	}
	defer sess.Close()

	// 1. Create platform if not exist
	platformName := strings.ReplaceAll(strings.ToLower(strings.TrimSpace(nodeInfo.Software.Name)), " ", "_")

	platform := getOrInsertPlatform(log, sess, &Platform{
		Name:       platformName,
		Homepage:   nodeInfo.Software.Homepage,
		Repository: nodeInfo.Software.Repository,
		Created:    crawled,
	})
	if platform == nil {
		return
	}

	// 2. check if we already have data
	dbNode := new(Node)
	nodeExist, err := sess.Where("domain = ?", domain).Get(dbNode)
	if err != nil {
		log.Error().Err(err).Msg("check if node exist")
		return
	}

	// 3. convert data
	newNode := newNode(nodeInfo, domain)
	newNode.ID = dbNode.ID
	newNode.PlatformID = platform.ID
	newNode.LastSuccessfulCrawl = crawled

	// 4. update or insert node
	if nodeExist {
		dbNode = updateNode(log, sess, newNode)
	} else {
		newNode.Proprietary = metadata.LookupProprietary(platform.Name)
		dbNode = insertNode(log, sess, newNode)
	}
	if dbNode == nil {
		return
	}

	// 5. save raw nodeinfo for future use cases
	dbNodeInfo := &NodeInfo{
		NodeID:    dbNode.ID,
		Timestamp: crawled,
		Data:      *nodeInfo,
	}
	if _, err := sess.Insert(dbNodeInfo); err != nil {
		log.Error().Err(err).Msgf("can not insert nodeinfo: %#v", dbNodeInfo)
		return
	}
	log.Trace().Msg("raw nodeinfo inserted")

	// 6. Create protocols new supported
	for _, prot := range nodeInfo.Protocols {
		p := &Protocol{NodeID: dbNode.ID, Name: prot}
		exist, err := sess.Exist(p)
		if err != nil {
			log.Error().Err(err).Msgf("can not check for protocol '%v'", p)
			return
		}
		if !exist {
			if _, err := sess.Insert(p); err != nil {
				log.Error().Err(err).Msgf("can not insert protocol '%v'", p)
				return
			}
			log.Trace().Msgf("protocol '%s' inserted", p.Name)
		} else {
			log.Trace().Msgf("protocol '%s' exist skip insert", p.Name)
		}
	}

	// 7. Delete protocols not supported anymore
	dbProts := make([]*Protocol, 0, 1)
	if err := sess.Where("node_id = ?", dbNode.ID).Find(&dbProts); err == nil {
		for _, dbProd := range dbProts {
			if !slices.Contains(nodeInfo.Protocols, dbProd.Name) {
				if _, err := sess.Delete(dbProd); err != nil {
					log.Error().Err(err).Msgf("could not delete protocol %v", dbProd)
					return
				}
				log.Trace().Msgf("protocol '%s' deleted for node", dbProd.Name)
			}
		}
	} else {
		log.Error().Err(err).Msg("could not find protocols for node")
		return
	}

	// 8. Create Services if not exist
	services := uniqSlices(nodeInfo.Services.Inbound, nodeInfo.Services.Outbound)
	dbServices := make(map[string]*Service)
	for _, s := range services {
		service := getOrInsertServie(log, sess, &Service{
			Name:    s,
			Created: crawled,
		})
		if service == nil {
			return
		}
		dbServices[s] = service
	}

	// 9. Create n-m links between node and service
	if _, err := sess.Where("node_id = ?", dbNode.ID).Delete(new(ServiceInbound)); err != nil {
		log.Error().Err(err).Msg("delete node related inbound services")
		return
	}
	if _, err := sess.Where("node_id = ?", dbNode.ID).Delete(new(ServiceOutbound)); err != nil {
		log.Error().Err(err).Msg("delete node related outbound services")
		return
	}
	for _, in := range nodeInfo.Services.Inbound {
		if _, err := sess.Insert(&ServiceInbound{NodeID: dbNode.ID, ServiceID: dbServices[in].ID}); err != nil {
			log.Error().Err(err).Msgf("insert node related inbound service '%s'", dbServices[in].Name)
			return
		}
	}
	for _, out := range nodeInfo.Services.Outbound {
		if _, err := sess.Insert(&ServiceOutbound{NodeID: dbNode.ID, ServiceID: dbServices[out].ID}); err != nil {
			log.Error().Err(err).Msgf("insert node related inbound service '%s'", dbServices[out].Name)
			return
		}
	}

	// 10. Insert Stat
	weekly, _ := nodeInfo.Metadata["usageUsersActiveWeek"].(int64)
	if _, err := sess.Cols("node_id", "timestamp", "users_total", "users_half_year", "users_monthly", "users_weekly",
		"local_posts", "local_comments", "inbound_services", "outbound_services").
		Insert(&Stat{
			NodeID:           dbNode.ID,
			Timestamp:        crawled,
			UsersTotal:       nodeInfo.Usage.Users.Total,
			UsersMonthly:     nodeInfo.Usage.Users.ActiveMonth,
			UsersHalfYear:    nodeInfo.Usage.Users.ActiveHalfyear,
			UsersWeekly:      weekly,
			LocalPosts:       nodeInfo.Usage.LocalPosts,
			LocalComments:    nodeInfo.Usage.LocalComments,
			InboundServices:  nodeInfo.Services.Inbound,
			OutboundServices: nodeInfo.Services.Outbound,
		}); err != nil {
		log.Error().Err(err).Msg("insert stat")
		return
	}
	log.Trace().Msg("stat data inserted")

	if err := sess.Commit(); err != nil {
		log.Error().Err(err).Msg("can not commit db session")
	}
	log.Trace().Msg("db session successfully committed")
}

// we might want to move this into fetch
func newNode(nodeInfo *nodeinfo.Node, domain string) *Node {
	var ip string
	ips, _ := net.LookupIP(domain)
	for i := range ips {
		if ip4 := ips[i].To4(); ip4 != nil && ip4.IsGlobalUnicast() {
			ip = ips[i].String()
		}
	}
	country := ""
	if ip != "" {
		// TODO(60): will not be updated anymore
		country = ip2location.Get_country_short(ip).Country_short
		if len(country) != 2 {
			log.Warn().Str("component", "database").Msgf("skip country, as ip2location returned code longer that 2 letters: %s", country)
			country = ""
		}
	}

	fullName, _ := nodeInfo.Metadata[known_extension.KeyName].(string)

	return &Node{
		Domain:      domain,
		FullName:    fullName,
		Version:     nodeInfo.Software.Version,
		Country:     country,
		IP:          ip,
		OpenSignups: nodeInfo.OpenRegistrations,
	}
}

func uniqSlices[S ~[]E, E comparable](a, b S) S {
	m := make(map[E]struct{})
	for _, i := range a {
		m[i] = struct{}{}
	}
	for _, i := range b {
		m[i] = struct{}{}
	}

	res := make([]E, 0, len(m))
	for k := range m {
		res = append(res, k)
	}
	return res
}
