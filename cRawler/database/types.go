package database

import (
	"time"

	"codeberg.org/thefederationinfo/nodeinfo-go"
)

type Database interface {
	GetCrawlList(limit int) (domains []string, err error)
	GetNode(domain string) (*Node, error)
	Insert(info *nodeinfo.Node, domain string, crawled Timestamp)
	Abandoned(domain string)
}

type Timestamp time.Time

type Platform struct {
	ID         int64 `xorm:"id"`
	Name       string
	Homepage   string
	Repository string
	Created    Timestamp `xorm:"created"`
}

type Service struct {
	ID      int64 `xorm:"id"`
	Name    string
	Created Timestamp
}

type Protocol struct {
	NodeID int64 `xorm:"node_id"`
	Name   string
}

type NodeInfo struct {
	NodeID    int64 `xorm:"node_id"`
	Timestamp Timestamp
	Data      nodeinfo.Node `xorm:"json"`
}

type Node struct {
	ID                  int64 `xorm:"id"`
	PlatformID          int64 `xorm:"platform_id"`
	Domain              string
	FullName            string
	Hide                bool
	Blocked             bool
	Version             string
	Country             string
	IP                  string `xorm:"ip"`
	OpenSignups         bool
	LastSuccessfulCrawl Timestamp
	Created             Timestamp `xorm:"created"`
	Updated             Timestamp `xorm:"updated"`
	Proprietary         bool
	Abandoned           bool
}

type Stat struct {
	ID               int64 `xorm:"id"`
	NodeID           int64 `xorm:"node_id"`
	Timestamp        Timestamp
	UsersTotal       int64
	UsersMonthly     int64
	UsersHalfYear    int64
	UsersWeekly      int64
	LocalPosts       int64
	LocalComments    int64
	InboundServices  []string `xorm:"json"`
	OutboundServices []string `xorm:"json"`
}

type ServiceInbound struct {
	ServiceID int64 `xorm:"service_id"`
	NodeID    int64 `xorm:"node_id"`
}

type ServiceOutbound struct {
	ServiceID int64 `xorm:"service_id"`
	NodeID    int64 `xorm:"node_id"`
}
