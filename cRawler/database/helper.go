package database

import (
	_ "github.com/lib/pq"
	"github.com/rs/zerolog"
	"xorm.io/xorm"
)

func getOrInsertPlatform(log zerolog.Logger, sess *xorm.Session, pl *Platform) *Platform {
	platform := new(Platform)
	platformExist, err := sess.Where("name = ?", pl.Name).Get(platform)
	if err != nil {
		log.Error().Err(err).Msg("platform check")
		return nil
	}
	if platformExist {
		log.Trace().Msgf("found platform: %#v", platform)
		return platform
	}

	// not found so create it
	if _, err := sess.Cols("name", "homepage", "repository", "created").Insert(platform); err != nil {
		log.Error().Err(err).Msgf("insert platform: %#v", pl)
		return nil
	}
	log.Info().Msgf("created new platform: %#v", platform)

	// well we need to get the generated id now so just load it all over again, as insert just wont handle ID :/
	if found, err := sess.Where("name = ?", pl.Name).Get(platform); err != nil || !found {
		log.Error().Err(err).Msg("not expected")
		return nil
	}
	return platform
}

func insertNode(log zerolog.Logger, sess *xorm.Session, newNode *Node) *Node {
	insertCols := []string{
		"created",
		"updated",
		"country",
		"domain",
		"full_name",
		"open_signups",
		"version",
		"platform_id",
		"last_successful_crawl",
		"blocked",
		"abandoned",
		"proprietary",
		"hide",
	}
	if newNode.IP != "" {
		insertCols = append(insertCols, "ip")
	}
	if _, err := sess.Cols(insertCols...).Insert(newNode); err != nil {
		log.Error().Err(err).Msgf("can not insert node '%#v'", newNode)
		return nil
	}
	log.Trace().Msg("new node inserted")

	// well we need to get the generated id now so just load it all over again, as insert just wont handle ID :/
	return getNode(log, sess, newNode.Domain)
}

func updateNode(log zerolog.Logger, sess *xorm.Session, node *Node) *Node {
	updateCols := []string{
		"domain",
		"platform_id",
		"full_name",
		// "country", // TODO(60): resolve country by IP
		"last_successful_crawl",
		"version",
		"open_signups",
		"abandoned",
	}
	if node.IP != "" {
		updateCols = append(updateCols, "ip")
	}
	if _, err := sess.Where("id = ?", node.ID).Cols(updateCols...).Update(node); err != nil {
		log.Error().Err(err).Msgf("can not update node '%#v'", node)
		return nil
	}
	log.Trace().Msg("node updated")

	return getNode(log, sess, node.Domain)
}

func getNode(log zerolog.Logger, sess *xorm.Session, domain string) *Node {
	node := new(Node)
	found, err := sess.Where("domain = ?", domain).Get(node)
	if err != nil || !found {
		log.Error().Err(err).Msg("not expected")
		return nil
	}
	return node
}

func getOrInsertServie(log zerolog.Logger, sess *xorm.Session, newService *Service) *Service {
	service := new(Service)
	if exist, err := sess.Where("name = ?", newService.Name).Get(service); err != nil {
		log.Error().Err(err).Msgf("could not get service '%s'", newService.Name)
		return nil
	} else if !exist {
		if _, err := sess.Cols("name", "created").Insert(newService); err != nil {
			log.Error().Err(err).Msgf("could not create service: %v", service)
			return nil
		}
		service = new(Service)
		// well we need to get the generated id now so just load it all over again, as insert just wont handle ID :/
		if find, err := sess.Where("name = ?", newService.Name).Get(service); err != nil || !find {
			log.Error().Err(err).Msg("not expected")
			return nil
		}
		log.Info().Msgf("created new service '%#v'", service)
	} else {
		log.Trace().Msgf("found service '%#v'", service)
	}
	return service
}
