module crawler

go 1.21

require (
	codeberg.org/thefederationinfo/nodeinfo-go v0.3.3-0.20240304130157-ad9368478bf5
	codeberg.org/thefederationinfo/nodeinfo_metadata_survey v0.0.0-20240228225804-df60a36dd120
	github.com/gammazero/deque v0.2.1
	github.com/go-chi/chi/v5 v5.0.10
	github.com/go-chi/render v1.0.3
	github.com/ip2location/ip2location-go v8.3.0+incompatible
	github.com/lib/pq v1.10.9
	github.com/rs/zerolog v1.31.0
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4
	xorm.io/builder v0.3.11-0.20220531020008-1bd24a7dc978
	xorm.io/xorm v1.3.7-0.20240101024435-4992cba040fe
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/goccy/go-json v0.8.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
