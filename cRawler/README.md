# Crawler

## QueueManager

- if DB.LatestQueueInsert != %date_now% -> Query DB for Nodes to be Updates
  - Push all Affected Nodes into "PullInfo"-Queue
  - Update LatestQueueInsert
- Pull "CrawledInfo"-Queue and Update/Insert DB
  - Check if Node exist in DB
    - NO:
      1. Check if plaform = Metadata.name exist -> use metadata.proprietary as default value for new node
      2. Insert Node, In-/Outbound Services, Protocolls, Stats Entry
    - YES
      1. Get related ode, In-/Outbound Services, Protocolls & Update if something changed
      2. Insert Stats Entry if stats for date dont exist
- Pull "NewNode"-Queue
  - Check if Node exist in DB
    - YES = noop
    - NO = push to "PullInfo"-Queue

## Worker

- Pull from "PullInfo"-Queue and get NodeInfo2
- Push result to "CrawledInfo"-Queue

## Notes

- PullInfo-Queue has "retry flag" witch worker do -1 and if current crawl did not work it's put bach into PullInfo with decremented flag
  if retry=1 and crawl fail, just drop it

## Configs

- `CRAWLER_DATABASE_URL`: SQL database connection
- `CRAWLER_LOG_LEVEL`: Set log level
- `CRAWLER_LOG_SQL`: Log SQL queries to logger
